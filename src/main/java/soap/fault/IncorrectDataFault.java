package soap.fault;

import javax.xml.ws.WebFault;

/**
 * Created by Admin on 02.11.2017.
 */
@WebFault(name = "IncorrectDataFault")
public class IncorrectDataFault extends Exception {
    private IncorrectDataFaultBean faultBean;
    public IncorrectDataFault(IncorrectDataFaultBean faultBean) {
        this.faultBean = faultBean;
    }

    public IncorrectDataFaultBean getFaultBean() {
        return faultBean;
    }

    public void setFaultBean(IncorrectDataFaultBean faultBean) {
        this.faultBean = faultBean;
    }
}
