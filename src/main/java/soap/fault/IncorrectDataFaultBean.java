package soap.fault;

/**
 * Created by Admin on 02.11.2017.
 */
public class IncorrectDataFaultBean {
    private String message;

    public IncorrectDataFaultBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
