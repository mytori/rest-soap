package soap.client;

import net.webservicex.*;

/**
 * Created by Admin on 02.11.2017.
 */
public class CurrencyConvertorClient {
    private  CurrencyConvertor convertor = new CurrencyConvertor();
    private  CurrencyConvertorSoap server = convertor.getCurrencyConvertorSoap();

    public double exchangeCurrency(double sum, Currency from, Currency to) {
        double rate = server.conversionRate(from, to);
        return sum * rate;
    }
}
