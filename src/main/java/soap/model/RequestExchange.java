package soap.model;

import net.webservicex.Currency;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 02.11.2017.
 */
@XmlRootElement(name="GetExchangeRequest")
public class RequestExchange {

    private double sum;
    private Currency from;
    private Currency to;

    public RequestExchange() {
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Currency getFrom() {
        return from;
    }

    public void setFrom(Currency from) {
        this.from = from;
    }

    public Currency getTo() {
        return to;
    }

    public void setTo(Currency to) {
        this.to = to;
    }
}
