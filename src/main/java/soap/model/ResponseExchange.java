package soap.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 02.11.2017.
 */
@XmlRootElement(name="GetExchangeResponse")
public class ResponseExchange {


    private double result;

    public ResponseExchange() {
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}
