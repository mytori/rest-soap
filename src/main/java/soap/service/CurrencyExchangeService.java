package soap.service;

import soap.fault.IncorrectDataFault;
import soap.model.RequestExchange;
import soap.model.ResponseExchange;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by Admin on 02.11.2017.
 */
@WebService
public interface CurrencyExchangeService {

    @WebMethod
    ResponseExchange getExchange(@WebParam(name = "RequestExchange")RequestExchange request)
            throws IncorrectDataFault;
}
