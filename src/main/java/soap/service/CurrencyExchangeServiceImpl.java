package soap.service;

import net.webservicex.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import soap.client.CurrencyConvertorClient;
import soap.fault.IncorrectDataFault;
import soap.fault.IncorrectDataFaultBean;
import soap.model.RequestExchange;
import soap.model.ResponseExchange;

/**
 * Created by Admin on 02.11.2017.
 */
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    @Autowired
    private CurrencyConvertorClient client;

    public ResponseExchange getExchange(RequestExchange request) throws IncorrectDataFault {
        Currency from = request.getFrom();
        Currency to = request.getTo();
        if (!isCurrencySupport(from) && !isCurrencySupport(request.getTo())) {
            throw new IncorrectDataFault(new IncorrectDataFaultBean("Supported currency: RUB, USD, EUR"));
        }
        ResponseExchange response = new ResponseExchange();
        response.setResult(client.exchangeCurrency(request.getSum(), from, to));
        return response;
    }
    private boolean isCurrencySupport(Currency currency) {
        return currency.equals(Currency.RUB)||currency.equals(Currency.USD)||currency.equals(Currency.EUR);

    }
}
