package rest.services;

import rest.model.Lesson;
import rest.model.Teacher;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Admin on 29.10.2017.
 */
@Produces("application/xml")
@Consumes("application/json")
public interface ITeacherService {
    @GET
    @Path("/teacher/{teacherId}")
    Response getTeacher(@PathParam("teacherId") long id);

    @POST
    @Path("/teacher")
    Response addTeacher(final Teacher teacher);

    @PUT
    @Path("/teacher/{teacherId}")
    Response  replaceTeacher(@PathParam("teacherId") long id, final Teacher teacher);

    @DELETE
    @Path("/teacher/{teacherId}")
    Response deleteTeacher(@PathParam("teacherId") long id);

    @GET
    @Path("/teacher/{teacherId}/lesson/{lessonId}")
    Response getLesson(@PathParam("teacherId") long teacherId, @PathParam("lessonId") long lessonId);

    @POST
    @Path("/teacher/{teacherId}/lesson")
    Response addLesson(@PathParam("teacherId") long id, Lesson lesson);

    @GET
    @Path("/teacher/busiest")
    Response getBusiestTeacher();

    @PUT
    @Path("/teacher/{teacherId}/lesson/{lessonId}")
    Response replaceLesson(@PathParam("teacherId") long teacherId, @PathParam("lessonId") long lessonId, Lesson lesson);

    @DELETE
    @Path("teacher/{teacherId}/lesson/{lessonId}")
    Response deleteLesson(@PathParam("teacherId") long teacherId, @PathParam("lessonId") long lessonId);
}
