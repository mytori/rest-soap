package rest.services;

import org.springframework.beans.factory.annotation.Autowired;
import rest.exception.ServerException;
import rest.repository.TeacherRepoImpl;
import rest.model.Lesson;
import rest.model.Teacher;
import rest.repository.TeacherRepository;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Admin on 29.10.2017.
 */
public class TeacherServiceImpl implements ITeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    public Response getTeacher(long id) {
        Teacher teacher = teacherRepository.read(id);
        if (teacher == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(teacher).build();
        }
    }

    public Response addTeacher(Teacher teacher) {
        teacherRepository.create(teacher);
        return Response.status(Response.Status.CREATED).entity(teacher).build();
    }


    public Response replaceTeacher(long id, final Teacher teacher) {
        Teacher currentTeacher = teacherRepository.read(id);
        if(teacher.equals(currentTeacher)) {
            return Response.notModified().build();
        }
        teacherRepository.update(id, teacher);
        return Response.noContent().build();
    }

    public Response deleteTeacher(long id) {
        teacherRepository.delete(id);
        return Response.noContent().build();
    }

    public Response getLesson(long teacherId, long lessonId) {
        Teacher teacher = teacherRepository.read(teacherId);
        List<Lesson> lessons = teacher.getLessons();
        for (Lesson lesson : lessons) {
            if (lesson.getId() == lessonId) {
                return Response.ok(lesson).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    public Response addLesson(long id, Lesson lesson) {
        Teacher teacher = teacherRepository.read(id);
        List<Lesson> lessons = teacher.getLessons();
        lessons.add(lesson);
        return Response.status(Response.Status.CREATED).entity(lesson).build();
    }

    public Response getBusiestTeacher() {
        int busiestDuration = -1;
        Teacher busiestTeacher = null;
        for (Teacher teacher : teacherRepository.getTeachers()) {
            int current = 0;
            for (Lesson lesson : teacher.getLessons()) {
                current += lesson.getDuration();
            }
            if (current > busiestDuration) {
                busiestDuration = current;
                busiestTeacher = teacher;
            }
        }
        return Response.ok(busiestTeacher).build();
    }

    public Response replaceLesson(long teacherId, long lessonId, Lesson lesson) {
        try{
            List<Lesson> lessons = teacherRepository.read(teacherId).getLessons();
            Lesson oldLesson = findLesson(lessons, lessonId);
            if (lesson.equals(oldLesson)) {
                return Response.notModified().build();
             }
            lessons.remove(oldLesson);
            lessons.add(lesson);
            return Response.noContent().build();
        } catch (NullPointerException e) {
            throw new ServerException(String.format("Teacher with %d id not found.", teacherId));
        }
    }

    public Response deleteLesson(long teacherId, long lessonId) {
        try {
            List<Lesson> lessons = teacherRepository.read(teacherId).getLessons();
            lessons.remove(findLesson(lessons, lessonId));
            return Response.noContent().build();
        } catch (NullPointerException e) {
            throw new ServerException(String.format("Teacher with %d id not found.", teacherId));
        }

    }
    private Lesson findLesson(List<Lesson> lessons, long id) {
        for (Lesson lesson : lessons) {
            if (lesson.getId() == id) {
                return lesson;
            }
        }
        throw new NullPointerException();
    }
}
