package rest.mapper;

import rest.exception.ServerException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by Admin on 02.11.2017.
 */
public class ServerExceptionMapper implements ExceptionMapper<ServerException> {
    public Response toResponse(ServerException e) {
        return Response.serverError().entity(e.getMessge()).build();
    }
}
