package rest.mapper;

import org.codehaus.jackson.map.JsonMappingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by Admin on 02.11.2017.
 */
public class ClientExceptionMapper implements ExceptionMapper<JsonMappingException> {
    public Response toResponse(JsonMappingException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
}
