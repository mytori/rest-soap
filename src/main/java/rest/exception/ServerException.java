package rest.exception;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 02.11.2017.
 */
@XmlRootElement
public class ServerException extends RuntimeException {

    private String message;

    public ServerException() {

    }
    public ServerException(final String msg) {
        message = msg;
    }

    public String getMessge() {
        return message;
    }
}
