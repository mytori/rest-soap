package rest.repository;

import rest.model.Teacher;

import java.util.Collection;

/**
 * Created by Admin on 02.11.2017.
 */
public interface TeacherRepository {
    Teacher read(long id);
    void create(Teacher teacher);
    void update(long id, Teacher teacher);
    void delete(long id);

    Collection<Teacher> getTeachers();

}
