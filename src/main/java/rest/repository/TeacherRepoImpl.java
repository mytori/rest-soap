package rest.repository;

import rest.model.Lesson;
import rest.model.Teacher;
import rest.services.ITeacherService;

import java.util.*;

/**
 * Created by Admin on 29.10.2017.
 */
public class TeacherRepoImpl implements TeacherRepository{

    private Map<Long, Teacher> teacherMap = new HashMap<Long, Teacher>();

    public Teacher read(long id) {
        return teacherMap.get(id);
    }

    public void create(final Teacher teacher) {
        teacherMap.put(teacher.getId(), teacher);
    }

    public void update(long id, Teacher teacher) {
        teacherMap.put(id, teacher);
    }

    public void delete(long id) {
        teacherMap.remove(id);
    }

    public Collection<Teacher> getTeachers() {
        return  teacherMap.values();
    }
}
