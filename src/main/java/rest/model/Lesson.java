package rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Admin on 29.10.2017.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Lesson {
    @XmlElement(required = true)
    private long id;
    private  String name;
    private int duration;

    public Lesson(long id, String name, int duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    public Lesson() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lesson lesson = (Lesson) o;

        if (id != lesson.id) return false;
        if (duration != lesson.duration) return false;
        return name != null ? name.equals(lesson.name) : lesson.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + duration;
        return result;
    }
}
